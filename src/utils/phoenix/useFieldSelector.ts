import {useSelector} from "react-redux";

interface IUseFieldSelector {
  formName: string;
  fieldName: string;
}

export const useFieldSelector = (props:IUseFieldSelector) => {
  return useSelector((state: any) => state.formController.formController?.[props.formName]?.[props.fieldName]?.data)
}