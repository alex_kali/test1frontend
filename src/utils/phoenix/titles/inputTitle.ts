import styled from "styled-components";

export const InputTitleStyled = styled.div`
  margin-top: 15px;
  margin-bottom: 4px;
  font-size: 18px;
  padding-left: 5px;
  padding-right: 5px;
  color: rgba(0,0,0,0.7);
`