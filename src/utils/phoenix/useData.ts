import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";

export function useDataSelector(selector: any, isLoading: boolean = false, getDefaultFunction: any = undefined, isGetDefaultData: boolean = true) {
  const dispatch = useDispatch()
  const data: any = useSelector(selector)

  useEffect(() => {
    if(data === null && getDefaultFunction && isGetDefaultData && !isLoading){
      dispatch(getDefaultFunction())
    }
  },[])

  return data
}
