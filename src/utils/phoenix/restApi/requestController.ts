import {AppThunk} from "../../../store/store";
import {wrapperFetch} from "./wrapperFetch";
import {loadingFailed, loadingSuccess, startLoading} from "./requestControllerSlice";

interface IRequestController {
  success?: (data: any) => void;
  error?: (errors: any) => void;
  serialize?: (data: any) => void;

  method: "POST" | "GET" | "DELETE" | "PUT";
  id: string;
  action: any;
  url: string;
  params?: object;
}

export const requestController = (props:IRequestController): AppThunk => async (
  dispatch: any
) => {
  dispatch(startLoading(props.id))
  try {
    const response = await wrapperFetch(props)
    const data = await response.json();
    dispatch(loadingSuccess(props.id))
    dispatch(props.action(data))
  } catch {
    dispatch(loadingFailed(props.id))
  }

}