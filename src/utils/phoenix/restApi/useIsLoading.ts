import {useSelector} from "react-redux";

export const useIsLoading = (name: string) => {
  return useSelector((state: any) => (state.requestController.isLoading as Array<string>).includes(name))
}