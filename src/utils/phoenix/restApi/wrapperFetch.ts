interface IWrapperFetch {
  url: string;
  method: "POST" | "GET" | "DELETE" | "PUT";
  params?: object;
}

export const wrapperFetch = async (props:IWrapperFetch) => {
  const params:any = {
    "headers": {
      'Accept': 'application/json',
      "content-type": "application/json",
    },
    "method": props.method,
    "mode": "cors",
    "credentials": "include"
  }
  if(props.params){
    params["body"] = JSON.stringify(props.params)
  }
  if(window.token){
    params["headers"]["Authorization"] = 'Token ' + window.token
  }
  return await fetch(process.env.REACT_APP_SERVER_API_URL + props.url, params);
}