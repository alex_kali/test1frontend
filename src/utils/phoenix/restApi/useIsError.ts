import {useSelector} from "react-redux";

export const useIsError = (name: string) => {
  return useSelector((state: any) => (state.requestController.isError as Array<string>).includes(name))
}