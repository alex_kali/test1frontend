import {createSlice, current} from "@reduxjs/toolkit";

export const initialState: any = {
  isLoading: [],
  isError: []
};

export const requestControllerSlice = createSlice({
  name: 'requestController',
  initialState,
  reducers: {
    startLoading: (state, action) => {
      const isErrorIndexOf = state.isError.indexOf(action.payload)
      if(isErrorIndexOf !== -1){
        state.isError.splice(isErrorIndexOf, 1);
      }

      state.isLoading.push(action.payload)
    },
    loadingSuccess: (state, action) => {
      const isLoadingIndexOf = state.isLoading.indexOf(action.payload)
      if(isLoadingIndexOf !== -1){
        state.isLoading.splice(isLoadingIndexOf, 1);
      }
    },
    loadingFailed: (state, action) => {
      const isLoadingIndexOf = state.isLoading.indexOf(action.payload)
      if(isLoadingIndexOf !== -1){
        state.isLoading.splice(isLoadingIndexOf, 1);
      }

      state.isError.push(action.payload)
    }
  },
});

export const { startLoading, loadingSuccess, loadingFailed } = requestControllerSlice.actions;

export const requestControllerReducer = requestControllerSlice.reducer;