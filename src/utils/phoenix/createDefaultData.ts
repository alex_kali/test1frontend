export interface ICreateDefaultData {
  data?: any;
  isLoading?: boolean;
  isError?: boolean;
}

export const createDefaultData = (props:ICreateDefaultData={}) => {
  return {
    data: props.data,
    isLoading: false,
    isError: false,
  }
}