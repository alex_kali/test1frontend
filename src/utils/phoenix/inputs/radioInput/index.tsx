import React, {FC, memo, useContext, useState} from 'react';

import {
  CustomCheckStyled,
  CustomRadioStyled,
  RadioInputStyled,
  RadioTextStyled,
  WrapperRadioInputStyled,
  WrapperRadioItemStyled
} from "./styled";
import {useField} from "react-redux-hook-form";

interface IRadioInput {
  name: string;
  data: Array<{text: string, value: string}>
  required?: boolean;
}

export const RadioInput: FC<IRadioInput> = (props) => {
  const {useData} = useField({
    name: props.name,
    isRequired: props.required,
  })
  const [data, changeData] = useData()

  return (
    <WrapperRadioInputStyled>
      {props.data.map((i)=>
        <label htmlFor={i.value} key={i.value}>
          <WrapperRadioItemStyled checked={data === i.value}>
            <RadioTextStyled>{i.text}</RadioTextStyled>
            <RadioInputStyled checked={data === i.value} id={i.value} type="radio" name={props.name} value={i.value} onChange={(e)=>{changeData(e.target.value)}}/>
            <CustomRadioStyled className={'radio'}><CustomCheckStyled /></CustomRadioStyled>
          </WrapperRadioItemStyled>
        </label>
      )}
    </WrapperRadioInputStyled>
  )
};