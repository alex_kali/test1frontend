import styled from "styled-components";
import {
  BORDER_INPUT,
  BORDER_RADIUS_INPUT, COLOR_INPUT, ERROR_BORDER_COLOR_INPUT, ERROR_COLOR_INPUT,
  FONT_SIZE_INPUT,
  HEIGHT_INPUT,
  MARGIN_INPUT, OPACITY_FOCUS_INPUT, OPACITY_INPUT, PADDING_INPUT,
  WIDTH_INPUT
} from "../../stylesConstant";
import InputMask from "react-input-mask";

interface ITimeInputStyled{
  isValidate: boolean;
}

export const TimeInputStyled = styled.input<ITimeInputStyled>`
  margin-top: ${MARGIN_INPUT};
  margin-bottom: ${MARGIN_INPUT};
  border-left: none;
  border-right: none;
  border-top: none;
  border-bottom: ${BORDER_INPUT};
  height: ${HEIGHT_INPUT};
  font-size: ${FONT_SIZE_INPUT};
  padding-left: 5px;
  padding-right: 5px;
  color: ${COLOR_INPUT};
  opacity: ${OPACITY_INPUT};
  width: 20px;
  text-align: center;
  margin-left: 15px;
  margin-right: 15px;
  &:focus{
    outline: none;
    opacity: ${OPACITY_FOCUS_INPUT};
  }
  ${props => `
    color: ${props.isValidate ? '' : ERROR_COLOR_INPUT} !important;
    border-color: ${props.isValidate ? '' : ERROR_BORDER_COLOR_INPUT} !important;
    margin-bottom: ${props.isValidate ? '' : '4px'} !important;
  `}
`

export const TextStyled = styled.div`
  
`

export const WrapperTimeInputStyled = styled.div`
  display: flex;
  align-items: center;
  max-width: 300px;
  padding-left: 10px;
  padding-right: 10px;
`