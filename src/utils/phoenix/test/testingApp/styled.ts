import styled from "styled-components";

export const WrapperTestingAppStyled = styled.div`
  position: fixed;
  width: 600px;
  height: 300px;
  border: 1px solid rgba(0,0,0,0.2);
  background: white;
  z-index: 999;
  
  display: flex;
  align-items: center;
  flex-direction: column;
`

export const InputStyled = styled.input`
  margin-top: 10px;
`

export const TitleStyled = styled.div`
  margin-top: 10px;
`

export const ButtonStyled = styled.button`
  border: 1px solid black;
  padding: 4px;
  display: block;
  margin-top: 10px;
`

export const BackgroundBlockStyled = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background: rgba(0,0,0,0.5);
  transform: none !important;
  z-index: 998;
`

export const ListActionsStyled = styled.div`
  width: 80%;
  height: calc(100% - 40px);
  border: 1px solid rgba(0,0,0,0.2);
  border-bottom: 0;
  overflow-y: auto;
`

export const ControlPanelStyled = styled.div`
  height: 40px;
`

export const TitleNameActionStyled = styled.div`
  margin-left: 20px;
  margin-top: 4px;
`

export const TitleNameActionGroupStyled = styled.div`
  float: left;
  margin-top: 10px;
  margin-right: 10px;
`

export const AddCheckBoxButtonStyled = styled.button`
  float: left;
  border: 1px solid black;
  padding: 4px;
  margin-top: 5px;
  margin-right: 10px;
  margin-left: 10px;
`

export const SaveGroupActionsButtonStyled = styled.button`
  float: left;
  border: 1px solid black;
  padding: 4px;
  margin-top: 5px;
  margin-right: 10px;
  margin-left: 10px;
`

export const SaveTestButtonStyled = styled.button`
  float: left;
  border: 1px solid black;
  padding: 4px;
  margin-top: 5px;
  margin-right: 10px;
  margin-left: 10px;
`