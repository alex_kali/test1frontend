import {FC, useState} from "react";
import {ButtonStyled, InputStyled, TitleStyled} from "./styled";

export const NameTestForm:FC<{onSubmit:any}> = (props) => {
  const [value, setValue] = useState('')
  return(
    <>
      <TitleStyled>Введите название теста</TitleStyled>
      <InputStyled onChange={(e) => {setValue(e.target.value)}}/>
      <ButtonStyled onClick={() => props.onSubmit(value)}>Сохранить</ButtonStyled>
    </>
  )
}