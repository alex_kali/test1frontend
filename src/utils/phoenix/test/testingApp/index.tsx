import {FC, useEffect, useState} from "react";
import {
  AddCheckBoxButtonStyled,
  BackgroundBlockStyled,
  ControlPanelStyled,
  ListActionsStyled, SaveGroupActionsButtonStyled, SaveTestButtonStyled, TitleNameActionGroupStyled,
  TitleNameActionStyled,
  WrapperTestingAppStyled
} from "./styled";
import Draggable from 'react-draggable';
import {NameTestForm} from "./nameTestForm";
import {GroupNameTestForm} from "./GroupNameTest";

export const TestingApp: FC = () => {
  const [actionList, setActionList] = useState<Array<any>>([])
  const [nameTest, setNameTest] = useState('')
  const [nameGroupTest, setNameGroupTest] = useState('')
  const [isBlock, setIsBlock] = useState(true)
  const [groupActionsList, setGroupActionsList] = useState<Array<any>>([])

  useEffect(()=>{
    const wrapper = document.getElementById('app') as HTMLBodyElement;

    wrapper.addEventListener('click', (event) => {
      let element = event.target as HTMLElement;
      const isButton = element.nodeName === 'BUTTON';
      if (isButton) {
        const action = {
          name: 'click on ' + element.id,
          event: 'click',
          id: element.id,
        }
        setActionList((actionList) => [...actionList,...[action]])
      }
    })

    wrapper.addEventListener('input', function (event:any) {
      let element = event.target as HTMLElement;
      if(event.data){
        const action = {
          name: 'add keyword "' + event.data + '" on ' + element.id,
          event: 'add keyword',
          id: element.id,
          data: event.data,
        }
        setActionList((actionList) => [...actionList,...[action]])
      }else{
        const action = {
          name: 'delete keyword' + ' on ' + element.id,
          event: 'delete keyword',
          id: element.id,
        }
        setActionList((actionList) => [...actionList,...[action]])
      }
    });
  },[])

  const saveActionGroup = () => {
    const groupActions = {
      name: nameGroupTest,
      group: actionList,
    }
    setGroupActionsList((groupList) => [...groupList,...[groupActions]])


    setNameGroupTest('')
    setIsBlock(true)
    setActionList([])
  }

  const saveTest = () => {
    console.log(groupActionsList)
  }

  return(
    <>
      <Draggable>
        <WrapperTestingAppStyled>
          {!isBlock &&
            <>
              <ControlPanelStyled>
                <TitleNameActionGroupStyled>{nameGroupTest}</TitleNameActionGroupStyled>
                <AddCheckBoxButtonStyled onClick={()=>setActionList((actionList) => [...actionList,...[{name: 'checkpoint', event: 'checkpoint'}]])}>Add checkpoint</AddCheckBoxButtonStyled>
                <SaveGroupActionsButtonStyled onClick={saveActionGroup}>Save action group</SaveGroupActionsButtonStyled>
                <SaveTestButtonStyled onClick={saveTest}>Save test</SaveTestButtonStyled>
              </ControlPanelStyled>

              <ListActionsStyled>
                {[...actionList].reverse().map((item =>
                    <TitleNameActionStyled>{item.name}</TitleNameActionStyled>
                ))}
              </ListActionsStyled>
            </>
          }
          {isBlock && !nameTest &&
            <NameTestForm onSubmit={(value: string) => setNameTest(value)}/>
          }
          {isBlock && nameTest &&
            <GroupNameTestForm onSubmit={(value: string) => {
              setNameGroupTest(value)
              setIsBlock(false)
            }}/>
          }
        </WrapperTestingAppStyled>
      </Draggable>
      {isBlock &&
        <BackgroundBlockStyled/>
      }
    </>
  )
}