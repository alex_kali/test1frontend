import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {ICreateDefaultData} from "./createDefaultData";

export const useOptions = (props:any) => {
  const dispatch = useDispatch()
  const data:any = {}
  for(let i in props){
    data[i] = (params: any) => {
      dispatch(props[i](params, data))
    }
  }

  return data
}

// const customEquality = (prevData:ICreateDefaultData, newData:ICreateDefaultData, deps:Array<'data' | 'isLoading' | 'isError'> | undefined) => {
//   if(!deps){
//     return prevData === newData
//   }else if(deps.length === 0){
//     return true
//   }
//   for(let i of deps){
//     if(prevData[i] !== newData[i]){
//       return false
//     }
//   }
//   return true
// }