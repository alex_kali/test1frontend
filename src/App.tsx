import React, {memo} from 'react';
import './App.css';
import {
  BrowserRouter as Router,
 useRoutes
} from "react-router-dom";
import {Auth} from "./pages/auth";
import {Contacts} from "./pages/contacts";
import {Header} from "./components/header";
import {Footer} from "./components/footer";
import {TestPage} from "./pages/test";

const App = memo(() => {
  const AppRoutes = memo(() => {
    return useRoutes([
      { path: "/", element: <Auth /> },
      { path: "contacts", element: <Contacts /> },
      { path: "test", element: <TestPage/> }
    ]);
  });

  return (
    <Router>
      <Header />
      <div style={{height: 'calc(100% - 160px)', overflow: "auto"}}>
        <AppRoutes />
      </div>
      <Footer />
    </Router>
  );
})

export default App;
