import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {store} from "./store/store";
import {Provider} from "react-redux";
import {TestingApp} from "./utils/phoenix/test/testingApp";
import {Dispatch} from "@reduxjs/toolkit";

declare global {
  interface Window {
    dispatch:any;
  }
}

window.dispatch = store.dispatch

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
    <Provider store={store}>
      <>
        {process.env.REACT_APP_IS_TESTING && <TestingApp />}
        <div style={{height: "100%"}} id={"app"}>
          <App />
        </div>
      </>
    </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
