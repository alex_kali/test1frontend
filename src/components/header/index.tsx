import {FC, memo, useEffect} from "react";
import {HeaderStyled, LogoutButtonStyled, UserNameStyled} from "./styled";
import {useToken} from "../../store/user/useToken";
import {User} from "../../store/user/model";

export const Header:FC = memo( () => {
  const [, setUserToken] = useToken()
  const userData = User.useData()
  return (
    <HeaderStyled>
      {userData &&
        <>
          <UserNameStyled>{userData.username}</UserNameStyled>
          <LogoutButtonStyled onClick={()=>{setUserToken('');User.options.logout()}}>Выйти</LogoutButtonStyled>
        </>
      }
    </HeaderStyled>
  )
})