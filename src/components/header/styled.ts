import styled from "styled-components";

export const HeaderStyled = styled.div`
  height: 80px;
  background: rgb(200,200,200);
`

export const UserNameStyled = styled.div`
  
`

export const LogoutButtonStyled = styled.div`
  background: white;
  width: 150px;
  height: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`