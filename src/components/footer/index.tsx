import {FC, memo} from "react";
import {FooterStyled} from "./styled";

export const Footer:FC = memo( () => {
  return (
    <FooterStyled>Footer</FooterStyled>
  )
})