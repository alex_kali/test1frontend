export const cascaderData = [
  {
    value: 'Иркутская область',
    text: 'Иркутская область',
    children: [
      {
        value: 'Иркутск',
        text: 'Иркутск',
        children: [
          {
            value: 'Улица 1',
            text: 'Улица 1',
          },
          {
            value: 'Улица 2',
            text: 'Улица 2',
          },
          {
            value: 'Улица 3',
            text: 'Улица 3',
          },
        ]
      },
      {
        value: 'Братск',
        text: 'Братск',
        children: [
          {
            value: 'Улица 1',
            text: 'Улица 1',
          },
          {
            value: 'Улица 2',
            text: 'Улица 2',
          },
          {
            value: 'Улица 3',
            text: 'Улица 3',
          },
        ]
      },
      {
        value: 'Ангарск',
        text: 'Ангарск',
        children: [
          {
            value: 'Улица 1',
            text: 'Улица 1',
          },
          {
            value: 'Улица 2',
            text: 'Улица 2',
          },
          {
            value: 'Улица 3',
            text: 'Улица 3',
          },
        ]
      },
    ]},
  {
    value: 'Хабаровский край',
    text: 'Хабаровский край'
  },
  {
    value: 'Красноярский край',
    text: 'Красноярский край'
  },
]
