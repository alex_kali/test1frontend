import styled from "styled-components";

export const WrapperTestStyled = styled.div`
  max-width: 500px;
  margin-left: auto;
  margin-right: auto;
  margin-top: 40px;
`