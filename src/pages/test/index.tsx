import {FC} from "react";
import {InputTitleStyled} from "../../utils/phoenix/titles/inputTitle";
import {PhoneInput} from "../../utils/phoenix/inputs/phoneInput";
import {PasswordInput} from "../../utils/phoenix/inputs/password";
import {TextInput} from "../../utils/phoenix/inputs/textInput";
import {WrapperTestStyled} from "./styled";
import {SubmitButton} from "../../utils/phoenix/buttons";
import {EmailInput} from "../../utils/phoenix/inputs/emailInput";
import {CheckboxInput} from "../../utils/phoenix/inputs/checkboxInput";
import {SingleCheckboxInput} from "../../utils/phoenix/inputs/singleCheckboxInput";
import {RadioInput} from "../../utils/phoenix/inputs/radioInput";
import {SelectInput} from "../../utils/phoenix/inputs/selectInput";
import {TimeInput} from "../../utils/phoenix/inputs/timeInput";
import {CascaderInput} from "../../utils/phoenix/inputs/cascaderInput";
import {cascaderData} from "./cascaderData";
import { useForm, WrapperForm } from "react-redux-hook-form";


export const TestPage:FC = () => {
  const form = useForm({name: 'testForm'})
  const onSubmit = (values:any) => {
    console.log(values)
  }

  return (
    <WrapperTestStyled>
      <WrapperForm form={form} onSubmit={onSubmit}>

        <CascaderInput name={'cascader'} data={cascaderData} title={'Cascader input'}/>

        <TimeInput name={'timeInput'} text={'Time'}/>

        <InputTitleStyled>Radio</InputTitleStyled>
        <RadioInput name={'radio'} data={[{value: '5', text: 'Checkbox 1'},{value: '6', text: 'Checkbox 2'},{value: '7', text: 'Checkbox 3'},{value: '8', text: 'Checkbox 4'}]}/>

        <InputTitleStyled>Single checkbox</InputTitleStyled>
        <SingleCheckboxInput text={'isData'} name={'singleCheckbox'} required/>

        <InputTitleStyled>Checkbox</InputTitleStyled>
        <CheckboxInput name={'checkbox'} data={[{value: '1', text: 'Checkbox 1'},{value: '2', text: 'Checkbox 2'},{value: '3', text: 'Checkbox 3'},{value: '4', text: 'Checkbox 4'}]} required/>

        <SelectInput title={'Select'} name={'select'} data={[{value: '11', text: 'Checkbox 1'},{value: '12', text: 'Checkbox 2'},{value: '13', text: 'Checkbox 3'},{value: '14', text: 'Checkbox 4'}]}/>

        <InputTitleStyled>Text</InputTitleStyled>
        <TextInput name={'text'} required/>

        <InputTitleStyled>Phone</InputTitleStyled>
        <PhoneInput name={'phone'} required/>

        <InputTitleStyled>Email</InputTitleStyled>
        <EmailInput name={'email'} required/>

        <InputTitleStyled>Password</InputTitleStyled>
        <PasswordInput name={'password'} required isValidateFunction/>

        <SubmitButton text={'Тестовая кнопка'}/>
      </WrapperForm>
    </WrapperTestStyled>
  )
}
