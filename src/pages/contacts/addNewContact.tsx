import {FC, memo} from "react";
import {AddContactButtonStyled, AddNewContactStyled, PhoneInputStyled, TextInputStyled} from "./styled";
import {Contact, ICreateContact} from "../../store/contact/model";
import {useForm, WrapperForm} from "react-redux-hook-form";

export const AddNewContact:FC = memo( () => {
  const form = useForm({name: 'newContact'})

  return (
    <AddNewContactStyled>
      <WrapperForm form={form} onSubmit={(data:ICreateContact) => {Contact.options.create(data);form.reset()}}>

        <TextInputStyled name={'name'} required/>

        <PhoneInputStyled name={'phone'} required/>

        <AddContactButtonStyled text={''}/>
      </WrapperForm>
    </AddNewContactStyled>
  )
})