import {FC, memo, useEffect, useState} from "react";
import {Contact, IContact} from "../../store/contact/model";
import {ContactStyled, WrapperContactsStyled, WrapperContactStyled} from "./styled";
import {AddNewContact} from "./addNewContact";
import {ContactView} from "./contact";
import {Search} from "./search";
import {useFieldSelector} from "../../utils/phoenix/useFieldSelector";

export const Contacts:FC = memo( () => {
  const contactData = Contact.useData()
  const search = useFieldSelector({formName: 'searchForm', fieldName: 'search'})
  if(contactData === null){
    return null
  }

  const filterContacts = () => {
    return  search ? contactData.filter((item:IContact)=>item.name.indexOf(search) === 0) : contactData
  }

  return (
    <WrapperContactStyled>
      <ContactStyled>
        <Search />
        <WrapperContactsStyled>
          {filterContacts().map((item:IContact) =>
            <ContactView data={item} key={item.id}/>
          )}
        </WrapperContactsStyled>
        <AddNewContact/>
      </ContactStyled>
    </WrapperContactStyled>
  )
})