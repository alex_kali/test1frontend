import styled from "styled-components";
import {PhoneInput} from "../../utils/phoenix/inputs/phoneInput";
import {TextInput} from "../../utils/phoenix/inputs/textInput";
import {SubmitButton} from "../../utils/phoenix/buttons";

export const WrapperContactStyled = styled.div`
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  min-height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const ContactStyled = styled.div`
  height: 600px;
  max-width: 500px;
  width: 100%;
  position: relative;
  &:before{
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    width: calc(100% - 2px);
    height: calc(100% - 2px);
    background: white;
    z-index: -1;
    border-radius: 22px;
    border: 1px solid rgba(0,0,0,0.1);
  }
`

export const AddNewContactStyled = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  height: 70px;
  width: 100%;
  border-top: 1px solid rgba(0,0,0,0.1);
  display: flex;
  align-items: center;
  justify-content: center;
  
`

export const UpdateContactStyled = styled.div`
  height: 70px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  
`

export const PhoneInputStyled = styled(PhoneInput)`
  width: calc(50% - 75px);
  float: left;
  margin-left: 10px;
  & + .error{
    display: none;
  }
`

export const TextInputStyled = styled(TextInput)`
  width: calc(50% - 75px);
  float: left;
  margin-left: 10px;
  & + .error{
    display: none;
  }
`

export const AddContactButtonStyled = styled(SubmitButton)`
  width: 40px;
  height: 40px;
  background: grey;
  margin-top: 0 !important;
`

export const WrapperContactsStyled = styled.div`
  height: calc(100% - 142px);
  overflow: auto;
`

export const ContactItemStyled = styled.div`
  width: 100%;
  height: 60px;
  background: rgba(230,230,230);
  margin-top: 10px;
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const NameStyled = styled.div`
  width: calc(50% - 75px);
  padding-left: 30px;
  padding-right: 10px;
`

export const PhoneStyled = styled.div`
  width: calc(50% - 75px);
  padding-left: 40px;
`

export const WrapperButtonsStyled = styled.div`
  width: 60px;
  height: 100%;
`

export const DeleteButtonStyled = styled.div`
  height: 50%;
  width: 100%;
  background: #8f0404;
  cursor: pointer;
  opacity: 0.5;
  &:hover{
    opacity: 1;
  }
`
export const UpdateButtonStyled = styled.div`
  height: 50%;
  width: 100%;
  background: #c47b0a;
  cursor: pointer;
  opacity: 0.5;
  &:hover{
    opacity: 1;
  }
`

export const SearchStyled = styled.div`
  height: 70px;
  border-bottom: 1px solid rgba(0,0,0,0.1);
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: 30px;
  padding-right: 30px;
`