import {FC, memo} from "react";
import {SearchStyled} from "./styled";
import {TextInput} from "../../utils/phoenix/inputs/textInput";
import {useForm, WrapperForm} from "react-redux-hook-form";

export const Search:FC = memo( () => {
  const form = useForm({name: 'searchForm'})
  return (
    <SearchStyled>
      <WrapperForm form={form}>
        <TextInput name={'search'}/>
      </WrapperForm>
    </SearchStyled>
  )
})