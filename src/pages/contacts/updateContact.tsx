import {FC, memo} from "react";
import {Contact, IContact} from "../../store/contact/model";
import {
  AddContactButtonStyled,
  PhoneInputStyled,
  TextInputStyled,
  UpdateContactStyled
} from "./styled";
import {useForm, WrapperForm} from "react-redux-hook-form";

export const UpdateContact:FC<{data:IContact, changeIsView: () => void}> = memo( ({data,changeIsView}) => {
  const form = useForm({name: `updateContact${data.id}`})

  return (
    <>
      <WrapperForm initialValue={data} form={form} onSubmit={(values:IContact) => {
        values.id = data.id
        Contact.options.update(values)
        changeIsView()
      }}>

        <TextInputStyled name={'name'} required isTouch/>

        <PhoneInputStyled name={'phone'} required isTouch/>

        <AddContactButtonStyled text={''}/>
      </WrapperForm>
    </>
  )
})