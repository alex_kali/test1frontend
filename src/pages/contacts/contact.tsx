import {FC, memo, useState} from "react";
import {
  ContactItemStyled,
  DeleteButtonStyled,
  NameStyled,
  PhoneStyled,
  UpdateButtonStyled,
  WrapperButtonsStyled
} from "./styled";
import {Contact, IContact} from "../../store/contact/model";
import {UpdateContact} from "./updateContact";

export const ContactView:FC<{data:IContact}> = memo( ({data}) => {
  const [isView, setIsView] = useState(true)
  return (
    <ContactItemStyled>
      {isView ? <>
        <NameStyled>{data.name}</NameStyled>
        <PhoneStyled>{data.phone}</PhoneStyled>
        <WrapperButtonsStyled>
          <DeleteButtonStyled onClick={()=>Contact.options.delete({id: data.id})}/>
          <UpdateButtonStyled onClick={()=>setIsView(false)}/>
        </WrapperButtonsStyled>
      </> : <>
        <UpdateContact data={data} changeIsView={()=>setIsView(true)}/>
      </>}
    </ContactItemStyled>
  )
})