import {FC, memo, useEffect, useState} from "react";
import {Login} from "./login";
import {
  ChoiceLeftStyled,
  ChoiceRightStyled,
  ContentStyled,
  WrapperAuthStyled, WrapperChoiceStyled,
  WrapperContentStyled
} from "./styled";
import {Registration} from "./registration";

export const Auth:FC = memo( () => {
  const [isLogin, changeIsLogin] = useState(true)

  return (
    <WrapperAuthStyled>
      <WrapperContentStyled>

        <WrapperChoiceStyled>
          <ChoiceLeftStyled checked={isLogin} onClick={()=>{changeIsLogin(true)}} id="authorization__button">Авторизация</ChoiceLeftStyled>
          <ChoiceRightStyled checked={!isLogin} onClick={()=>{changeIsLogin(false)}} id="registration__button">Регистрация</ChoiceRightStyled>
        </WrapperChoiceStyled>
        <ContentStyled >
          {isLogin && <Login />}
          {!isLogin && <Registration />}
        </ContentStyled>
      </WrapperContentStyled>
    </WrapperAuthStyled>
  )
})