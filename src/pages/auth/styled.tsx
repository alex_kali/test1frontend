import styled from "styled-components";

export const WrapperAuthStyled = styled.div`
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  min-height: 80%;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const WrapperChoiceStyled = styled.div`
  width: 100%;
  height: 60px;
  clear: both;
`

export const WrapperContentStyled = styled.div`
  max-width: 500px;
  width: 100%;
  position: relative;
  &:before{
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    width: calc(100% - 2px);
    height: calc(100% - 2px);
    background: white;
    z-index: -1;
    border-radius: 22px;
    border: 1px solid rgba(0,0,0,0.1);
  }
`

export const ContentStyled = styled.div`
  width: calc(100% - 82px);
  background: rgb(253,251,250);
  overflow: hidden;
  border: 1px solid rgba(0,0,0,0.1);
  padding-left: 40px;
  padding-right: 40px;
  padding-bottom: 30px;
  border-bottom-right-radius: 20px;
  border-bottom-left-radius: 20px;
`

interface IChoice {
  checked: boolean;
}

export const ChoiceLeftStyled = styled.button<IChoice>`
  width: calc(50% - 1px);
  height: 100%;
  float: left;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  position: relative;
  font-size: 18px;
  ${props => props.checked ? `
    border-left: 1px solid rgba(0,0,0,0.1);
    border-right: 1px solid rgba(0,0,0,0.1);
    border-top: 1px solid rgba(0,0,0,0.1);
    background: rgb(253,251,250);
    margin-bottom: -1px;
    height: calc(100% + 1px);
  ` : ''}
`

export const ChoiceRightStyled = styled.button<IChoice>`
  width: calc(50% - 1px);
  height: 100%;
  float: left;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  font-size: 18px;
  ${props => props.checked ? `
    border-left: 1px solid rgba(0,0,0,0.1);
    border-right: 1px solid rgba(0,0,0,0.1);
    border-top: 1px solid rgba(0,0,0,0.1);
    background: rgb(253,251,250);
    margin-bottom: -1px;
    height: calc(100% + 1px);
  ` : ''}
`

