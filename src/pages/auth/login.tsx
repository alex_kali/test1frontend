import {FC, memo} from "react";
import {PasswordInput} from "../../utils/phoenix/inputs/password";
import {PhoneInput} from "../../utils/phoenix/inputs/phoneInput";
import {InputTitleStyled} from "../../utils/phoenix/titles/inputTitle";
import {SubmitButton} from "../../utils/phoenix/buttons";
import {ILogin, User} from "../../store/user/model";
import {useForm, WrapperForm} from "react-redux-hook-form";

export const Login:FC = memo( () => {
  const form = useForm({name: 'login'})

  return (
    <WrapperForm form={form} onSubmit={(data:ILogin) => {User.options.login(data)}}>
      <InputTitleStyled>Номер телефона</InputTitleStyled>
      <PhoneInput name={'username'} required/>

      <InputTitleStyled>Пароль</InputTitleStyled>
      <PasswordInput name={'password'} required/>

      <SubmitButton text={'Авторизироваться'}/>
    </WrapperForm>
  )
})