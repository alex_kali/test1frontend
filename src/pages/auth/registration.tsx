import {FC, memo, useEffect} from "react";
import {InputTitleStyled} from "../../utils/phoenix/titles/inputTitle";
import {PhoneInput} from "../../utils/phoenix/inputs/phoneInput";
import {PasswordInput} from "../../utils/phoenix/inputs/password";
import {SubmitButton} from "../../utils/phoenix/buttons";
import {EmailInput} from "../../utils/phoenix/inputs/emailInput";
import {IRegistration, User} from "../../store/user/model";
import {useForm, WrapperForm} from "react-redux-hook-form";

export const Registration:FC = memo( () => {
  const form = useForm({name: 'registration'})

  const password1 = form.useFieldSelector('password')
  const password2 = form.useFieldSelector('password2')

  useEffect(()=>{
    if(password1 !== password2){
      form.changeIsValidateField('password2',false)
      form.changeMessageErrorField('password2', 'Поля должны совпадать')
    }
  },[password1, password2])

  return (
    <WrapperForm form={form} onSubmit={(data:IRegistration) => {User.options.registration(data)}}>
      <InputTitleStyled>Номер телефона</InputTitleStyled>
      <PhoneInput name={'username'} required/>

      <InputTitleStyled>Емайл</InputTitleStyled>
      <EmailInput name={'email'} required/>

      <InputTitleStyled>Пароль</InputTitleStyled>
      <PasswordInput name={'password'} required isValidateFunction/>

      <InputTitleStyled>Повторите пароль</InputTitleStyled>
      <PasswordInput name={'password2'} required/>

      <SubmitButton text={'Авторизироваться'}/>
    </WrapperForm>
  )
})