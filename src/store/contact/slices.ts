import {createSlice, current} from "@reduxjs/toolkit";
import {createDefaultData} from "../../utils/phoenix/createDefaultData";
import {IContact} from "./model";

export const initialState: any = {
  contact: null,
};


export const contactSlice = createSlice({
  name: 'contact',
  initialState,
  reducers: {
    setContact: (state, action) => {
      state.contact = action.payload
    },
    addContact: (state, action) => {
      state.contact = [...current(state.contact),action.payload]
    },
    deleteContact: (state, action) => {
      state.contact = current(state.contact).filter((item:IContact)=>item.id != action.payload.id)
    },
    updateContact: (state, action) => {
      state.contact = current(state.contact).map((item:IContact)=> item.id === action.payload.id ? action.payload : item)
    },
  },
});

export const { updateContact, setContact, addContact, deleteContact } = contactSlice.actions;

export default contactSlice.reducer;