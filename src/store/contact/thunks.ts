import {AppThunk} from "../store";
import {requestController} from "../../utils/phoenix/restApi/requestController";
import {addContact, deleteContact, setContact, updateContact} from "./slices";
import {ICreateContact, IDeleteContact, IUpdateContact} from "./model";

export const getContactData = (): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getContactData`,
    url: '/contact/',
    method: 'GET',
    action: setContact,
  }))
};

export const createContact = (data:ICreateContact): AppThunk => async (
  dispatch: any
) => {

  dispatch(requestController({
    id: `createContact`,
    url: '/contact/',
    method: 'POST',
    action: addContact,
    params: data,
  }))
};

export const deleteContactData = (data:IDeleteContact): AppThunk => async (
  dispatch: any
) => {

  dispatch(requestController({
    id: `deleteContactData${data.id}`,
    url: `/contact/${data.id}/`,
    method: 'DELETE',
    action: deleteContact,
  }))
};

export const updateContactData = (data:IUpdateContact): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `updateContactData${data.id}`,
    url: `/contact/${data.id}/`,
    method: 'PUT',
    action: updateContact,
    params: data,
  }))
};