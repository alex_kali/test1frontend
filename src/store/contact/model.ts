import {useSelector} from "react-redux";
import {RootState, store} from "../store";
import {useDataSelector} from "../../utils/phoenix/useData";
import {createContact, deleteContactData, getContactData, updateContactData} from "./thunks";
import {IUser} from "../user/model";
import {useIsLoading} from "../../utils/phoenix/restApi/useIsLoading";
import {useIsError} from "../../utils/phoenix/restApi/useIsError";

export const Contact = {
  useData: () => useDataSelector(
    (state: RootState) => state.contact.contact,
    Contact.object.getIsLoading(),
    getContactData
  ) as Array<IContact> | null,
  useIsLoading: () => useIsLoading('getContactData'),
  useIsError: () => useIsError('getContactData'),

  object: {
    getData: () => store.getState().contact.contact as Array<IContact> | null,
    getIsLoading: () => false,
  },
  options: {
    create: (data:ICreateContact) => window.dispatch(createContact(data)),
    update: (data:IUpdateContact) => window.dispatch(updateContactData(data)),
    delete: (data:IDeleteContact) => window.dispatch(deleteContactData(data)),
  }
}

export interface IContact {
  id: string | number;
  phone: string;
  name: string;

  user?: IUser;
}

//****************** Options interface ******************//

export interface ICreateContact {
  phone: string;
  name: string;
}

export interface IUpdateContact {
  id?: string | number;
  phone: string;
  name: string;
}

export interface IDeleteContact {
  id: string | number;
}