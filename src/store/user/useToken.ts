import {useDispatch, useSelector} from "react-redux";
import useCookie, {updateItem} from 'react-use-cookie';
import {useEffect} from "react";
import {useAccess} from "../../utils/phoenix/useAccess";
import {User} from "./model";
import { useBeforeRender } from "react-redux-hook-form";

declare global {
  interface Window {
    token: string | undefined;
  }
}

export const useToken = () => {
  const [userToken, setUserToken] = useCookie('token');
  const token = useSelector((state: any) => state.user.user?.token)

  useBeforeRender(()=>{
    if(userToken && !token){
      window.token = userToken
      User.options.getData()
    }
  })

  useEffect(()=>{
    if(token){
      setUserToken(token)
      window.token = token
    }
  }, [token])

  useAccess({
    isLoginUser: !!userToken,
    paths: ['/'],
    redirectAuthorized: 'contacts',
    redirectUnauthorized: '/'
  })

  return [userToken, setUserToken as any]
}