import {useOptions} from "../../utils/phoenix/useOptions";
import {RootState, store} from "../store";
import {getUserData, login, logout, registration} from "./thunks";
import {useSelector} from "react-redux";
import {useDataSelector} from "../../utils/phoenix/useData";
import {useIsLoading} from "../../utils/phoenix/restApi/useIsLoading";
import {useIsError} from "../../utils/phoenix/restApi/useIsError";

export const User = {
  useData: () => useDataSelector((state: RootState) => state.user.user) as IUser | null,
  useIsLoading: () => useIsLoading('getUserData'),
  useIsError: () => useIsError('getUserData'),
  object: {
    getData: () => store.getState().user.user as IUser | null,
  },
  options: {
    login: (data:ILogin) => window.dispatch(login(data)),
    registration: (data:IRegistration) => window.dispatch(registration(data)),
    getData: () => window.dispatch(getUserData()),
    logout: () => window.dispatch(logout()),
  },
}

export interface IUser {
  username?: string;
  email?: string;
  token?: string;
}

//****************** Options interface ******************//

export interface ILogin {
  username: string;
  password: string;
}

export interface IRegistration {
  username: string;
  email: string;
  password: string;
}
